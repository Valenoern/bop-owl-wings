;;; rdf test  {{{
;; tested with cl-rdfxml ver 2014-07-13
(asdf:load-system "cl-rdfxml") (asdf:load-system "puri")
(asdf:load-system "conversion-thingy")

(defpackage :owlwing-test
	(:use :common-lisp :bop-debug
		:conversion-thing-util :conversion-thing-dict
		:conversion-thing-table :conversion-thing-turtle
	)
	(:local-nicknames  (:rdfxml :cl-rdfxml))
	;; actual package names aren't really supposed to be prefixed with cl yet this one is
)
(in-package :owlwing-test)
;;; }}}

;; TODO: find a way to pre-compile xml schema into lisp data, to use as a lisp system


(defun xml-to-turtle (xml-file  &key dict) ; {{{
	(let (input triple-table turtle)
	
	;; load xml file
	(setq input (uiop:read-file-string (uiop:ensure-pathname xml-file)))
	
	;; make a table that for now represents the b:cite element
	(setq triple-table (xml-to-table input))
	;; convert to turtle
	(setq turtle (table-to-turtle triple-table dict))
	
	;; output turtle
	(princ turtle)
)) ; }}}

(defun schema-test (xml-file) ; turn a schema file into a dict {{{
	(let (input triple-table dict)
	
	;; load xml schema
	(setq input (uiop:read-file-string (uiop:ensure-pathname xml-file)))
	;; make a table that for now defines the b:cite element
	(setq triple-table (xml-to-table input))
	(setq dict (table-to-dict triple-table))
	
	dict
)) ; }}}

(defun main () ; {{{
	(let ((args (uiop:command-line-arguments)) schema)
	(let ((wants (nth 1 args)) (action (nth 2 args)))
	
	(cond
		((not (null action))
			(setq schema (schema-test action))
			(xml-to-turtle wants :dict schema)
		)
		;((not (null wants))
		;	(xml-to-turtle wants)
		;)
		(t (format t "Please specify a file"))
	)
	
))) ; }}}

(main)
