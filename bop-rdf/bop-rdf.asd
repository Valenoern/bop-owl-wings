(defsystem "bop-rdf"
	:depends-on ("uiop" "bopwiki" "conversion-thingy")
	:components (
		(:file "inline")  ; render entries for 'bop publish' as html
		(:file "publish")  ; for exporting to csv - currently just for pickerquest
		
		;; re-exports smaller packages into "bop-rdf" package
		(:file "bop-rdf-pkg" :depends-on ("inline" "publish"))
))

;; to use this system in a bop folder, add it to init.lisp:
;;   (asdf:load-system "bop-rdf")
;;
;; also activate the pickerquest exporter:
;;   (default-publish-format 'bop-rdf:publish-bop-pickerquest)
;;
;; bop is not yet ready to specify different export formats for different directories,
;; though that would be a more ideal way for things to work and is planned eventually.
