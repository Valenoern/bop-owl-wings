;;; for exporting rdf entries to pickerquest {{{
(defpackage :bop-rdf-pickerquest
	(:use :common-lisp :bopwiki :bop-render-core
		:conversion-thing-table
	)
	(:local-nicknames (:table :bopwiki) (:rdfxml :cl-rdfxml)
		)
	(:export
		#:*publish-pickerquest-path*
		#:publish-bop-pickerquest
))
(in-package :bop-rdf-pickerquest)
;;; }}}


;;; config functions {{{

(defparameter *publish-pickerquest-path* (parse-namestring "/tmp/bop/")) ; {{{

(defun publish-pickerquest-path(path)
"Set directory to publish rendered entries to when running 'bop publish', using the lisp pathname PATH.
This function is designed to be used in init.lisp, and to nest any arbitrary function to calculate the pathname, ex. (parse-namestring \"/path/to/output\")"
	(let ((path-object (uiop:ensure-pathname path)))
	;; BUG: check?
	(setq *publish-pickerquest-path* path-object)
))
;; }}}

;;; }}}

(defun normalise-value (value cell) ; {{{
	(cond
		;; use empty string instead of the string "NIL"
		((null value)  "")
		;; simplify item ID down to fragment
		((and (equal cell "http://www.w3.org/1999/02/22-rdf-syntax-ns#subject")
			(typep value 'puri:uri)
			)
			(puri:uri-fragment value)
		)
		(t value)
)) ; }}}

(defun add-to-result (cell result) ; {{{
	;; helper to tidy up xml-to-csv
	(if (null result)
		(format nil "~a" cell)
		(format nil "~a,~a" result cell)
	)
) ; }}}

(defun xml-to-csv (xml headings) ; {{{
	(let (table result)
	(setq table (conversion-thing-table:xml-to-table xml))
	
	;; loop through items in table
	(maphash #'(lambda (item props)
		item  ; leave dummy alone
		
		;; if key contains hash table representing item (it usually should)
		(when (hash-table-p props)
			(dolist (cell headings)
				(let ((value
					(normalise-value (gethash cell props) cell)
				))
				;; add each property to result in order
				(setq result (add-to-result value result))
				)
		))
	) table)
	
	result
)) ; }}}

(defun pq-headers () ; full IRIs of columns in order {{{
	(list
		"http://www.w3.org/1999/02/22-rdf-syntax-ns#subject"
		"http://distributary.network/owl-wing/2021-06/pickerquest#article"
		"http://distributary.network/owl-wing/2021-06/pickerquest#name"
		"http://distributary.network/owl-wing/2021-06/pickerquest#form"
		"http://distributary.network/owl-wing/2021-06/pickerquest#cp"
		"http://distributary.network/owl-wing/2021-06/pickerquest#quadrant"
)) ; }}}

(defun publish-bop-pickerquest (wants pwd) ; {{{
"publish rdf entries to pickerquest csv format"
	(let (output non-revision (pwd (surer-pathname pwd)) result
		(csv-file (make-pathname :name "contestants" :type "csv"))
	)
	
	;; create directory, & export to user-configured directory
	(setq output (uiop:ensure-pathname *publish-pickerquest-path*))
	(ensure-directories-exist output)
	
	;; get known pages
	(find-all-pages pwd)
	(setq non-revision (parse-wants wants pwd))
	;; "Exporting DIR as FORMAT to /output/dir/"
	(export-announce :wants wants :fmt-name "pickerquest" :output output)
	
	;; LATER:
	;; - get this from a schema / 'owlwing' somehow
	;; - use the bop-rdf abbreviator to get short names
	(setq result "id,article,name,form,cp,quadrant")
	
	;; render individual non-revision pages
	(dolist (pagename non-revision)
		(let (body ext)
		;; temporarily get full tablerow
		(let ((full-body (page-body pagename)))
			(setq ext (bop-page-ext full-body))
			(setq body (bop-page-body full-body))
		)
		
		;; LATER: do a more sophisticated test
		(when (equal ext "xml")
			(setq body (join-str-with body (nn)))
			(setq result (format nil "~a~%~a" result
				(xml-to-csv body (pq-headers))
			))
		)
	))
	
	;; export result csv file
	(export-in-dir output csv-file result)
)) ; }}}
