;;; for outputting rdf bop entries like regular ones {{{
(defpackage :bop-rdf-inline
	(:use :common-lisp :bopwiki
		:conversion-thing-table
	)
	(:local-nicknames (:table :bopwiki) (:rdfxml :cl-rdfxml)
		)
	(:export
		#:dummy-symbol
))
(in-package :bop-rdf-inline)
;;; }}}
