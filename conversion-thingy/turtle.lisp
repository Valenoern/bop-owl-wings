;;; for outputting rdf turtle from a hash table {{{
(defpackage :conversion-thing-turtle
	(:use :common-lisp :cl-rdfxml :bop-debug
		:conversion-thing-util :conversion-thing-table :conversion-thing-dict
	)
	(:local-nicknames  (:rdfxml :cl-rdfxml))
		;; actual package names aren't really supposed to be prefixed with cl yet this one is
	(:export
		#:format-property-turtle #:table-to-turtle
))
(in-package :conversion-thing-turtle)
;; }}}


;;; print individual lines - format-property-turtle , format-prefix- {{{

(defun format-property-turtle (predicate value) ; print individual turtle property {{{
"try to represent a single property in rdf turtle given an already shortened PREDICATE name string and a typed VALUE"
	(let ((value-type (type-of value)) (key predicate) flat-value)
	
	(when (typep key 'property)  (setq key (flatten-property key)))
	
	(setq flat-value
	(cond
		;; if value is a uri, put in brackets
		((equal value-type 'puri:uri)
			(format nil "<~a>"
				(puri:render-uri value nil))
		)
		;; if value is classified a literal by rdfxml, get literal contents
		((equal value-type 'rdfxml:plain-literal)
			(format nil "\"~a\""
				(rdfxml:literal-string value))
		)
		;; otherwise stringify the value exactly as it is
		(t (format nil "~a" value))
	))
	
	(format nil "~a ~a" key flat-value)
)) ; }}}

(defun format-prefix-turtle (abbr prefix) ; print top prefix lines {{{
	(format nil "@prefix ~a: <~a> ." abbr prefix)
) ; }}}

;;; }}}


(defun internal-property-p (predicate) ; check for internally used property {{{
"return T if the uri PREDICATE is an internally-used rdf property that shouldn't be output to turtle"
	(equal predicate "http://www.w3.org/1999/02/22-rdf-syntax-ns#subject")
) ; }}}

(defun owl-prefix-p (datatype) ; check if row is a prefix {{{
	(unless (null datatype)
		(puri:uri= datatype (puri:uri "http://www.w3.org/2002/07/owl#Prefix")))
		;(equal datatype "http://www.w3.org/2002/07/owl#Prefix"))
) ; }}}

(defun table-to-turtle (table dictionary) ; {{{
	(let ((prefixes "") (result "") (prefix-table (make-hash-table :test 'equal))
		)
	;
	;(debug-dictionary dictionary)
	
	;; loop through items in table
	(maphash #'(lambda (item-name v)
		;; always assume item is named in uri fragment - will probably change later
		(let ((item-uri (puri:uri item-name)) fragment item-id)
			(setq fragment (format nil "#~a" (puri:uri-fragment item-uri)))
			(setq item-id  (if (null fragment)  item-name fragment))
			(setq result (string-add-line result (format nil "~%<~a>" item-id)))
		)
		
		;; for each item print properties
		(when (hash-table-p v)
		(maphash #'(lambda (s o)
			(let ((prop (gethash (string s) dictionary)) short-prefix prefix-row)
			
			(cond
			((null prop)  (setq prop s))
			(t
				(setq prefix-row (gethash (property-prefix prop) dictionary))
				(unless (null prefix-row)
					(setq short-prefix (property-name prefix-row))
					;; change :prefix to abbreviated
					(setf (property-prefix prop) short-prefix)
				)
				(puthash short-prefix (property-prefix prefix-row) prefix-table)
			))
			
			;; when s is an internally-used property, exclude it from turtle
			(unless (internal-property-p s)
			(setq result (string-add-line result
				(format nil "  ~a" (format-property-turtle prop o))
			)))
		)) v)
			
		(setq result (string-add-line result (format nil ".")))
		)
	)
	table)
	
	;; stringify prefixes
	(maphash #'(lambda (abbr prefix)
		(setq prefixes (string-add-line prefixes (format-prefix-turtle abbr prefix)))
	) prefix-table)
	
	(setq result (string-add-line prefixes result))
	
	result
	
	#| intended ttl looks something like:
@prefix :     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix bop:  <http://distributary.network/owl-wing/2021/bop#> .

<valenoern@distributary.network>
  created "2020-05-01T01:39:12Z"
  title "valenoern@distributary.network"
  name "Valenoern"
  username "valenoern"
  domain "distributary.network"
.
	|#
)) ; }}}
