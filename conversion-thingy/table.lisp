;;; hash table for parsed triples  {{{
(defpackage :conversion-thing-table
	(:use :common-lisp :cl-rdfxml :bop-debug
		:conversion-thing-util :conversion-thing-dict
	)
	(:local-nicknames
		;; actual package names aren't really supposed to be prefixed with cl yet this one is
		(:rdfxml :cl-rdfxml)
		(:flexi :flexi-streams)
	)
	(:export
		;; common-prefix rdf-name
		#:xml-to-table  ; xml-to-table-inner
		#:debug-table
		
		#:file-to-typed-item
))
(in-package :conversion-thing-table)
;;; }}}


;;; helpers {{{

(defun common-prefix (vocab) ; {{{
"return full prefix uri for vocabulary VOCAB, as long as it is a very common one.
these include: rdf"
	(cond
		((equal vocab "rdf")  "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
		(t "")
)) ; }}}

(defun rdf-name (name &key prefix vocab) ; {{{
	;; try to get full prefix
	(let ((prefix-uri (common-prefix vocab))
		)
	prefix  ; leave dummy alone
	
	(format nil "~a~a" prefix-uri name)
)) ; }}}

;;; }}}


(defun xml-to-table-inner (xml) ; {{{
"convert rdf/xml string to hash table of all items"
	(let ((big-table (make-hash-table :test 'equal)))
	
	(rdfxml:parse-document  (lambda (subject predicate object)
		(let (flat-subject flat-predicate property-table)
		
		;; treat blank nodes and labelled nodes differently
		(cond
			((equal (type-of subject) 'rdfxml:blank-node)
				;;(printl "BLANK NODE found" (type-of subject) predicate object)
				'()
			)
			(t
				(setq flat-subject (puri:render-uri subject nil))
				(setq flat-predicate (puri:render-uri predicate nil))
		))
		(print (list "PARSE XML" subject predicate object))
		
		;; try to get item table from big table, or create it
		(setq property-table (gethash flat-subject big-table))
		(when (null property-table)
			;; if item table not found, create it
			(setq property-table (make-hash-table :test 'equal))
			;; add item's id into table
			(puthash (rdf-name "subject" :vocab "rdf") subject property-table)
			;; put item in big table
			(puthash flat-subject property-table big-table)
		)
		
		;; convert literals to string
		;; LATER: move elsewhere?
		(when (typep object 'literal)
			(setq object (rdfxml:literal-string object))
		)
		
		;; put triple in object table
		(puthash flat-predicate object property-table)
		
	)) xml)
	
	big-table  ; return table
	
	#| example xml:
	
<?xml version="1.0" encoding="UTF-8"?>
<rdf:RDF
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	xmlns:b="http://distributary.network/owl-wing/2021/bop#">

<b:cite rdf:ID="valenoern_at_distributary.network-cite">
	<b:created>2020-05-01T01:39:12Z</b:created>
	<b:title>valenoern@distributary.network</b:title>
	<b:name>Valenoern</b:name>
	<b:username>valenoern</b:username>
	<b:domain>distributary.network</b:domain>
</b:cite>

</rdf:RDF>

	example triples:
	
"/#valenoern_at_distributary.network-cite" "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" #<PURI:URI http://distributary.network/owl-wing/2021/bop#cite>
"/#valenoern_at_distributary.network-cite" "http://distributary.network/owl-wing/2021/bop#created" #<"2020-05-01T01:39:12Z">
...
	|#
)) ; }}}

(defun xml-to-table (xml-data) ; wrapper to pass octets {{{
;; docstring {{{
"Convert XML-DATA to rdf table.

This function is a wrapper for XML-TO-TABLE-INNER to always pass it an octet array; this is what cl-rdfxml requires, but bopwiki will most likely pass a string.

XML - a document to be parsed by rdfxml, either as a UTF-8 encoded string or an octet format \"appropriate for cxml:make-source\""
;; }}}

	(if (stringp xml-data)
		;; if string, turn into an octet array
		;; the string is currently assumed to be utf-8
		(xml-to-table-inner
			(flexi:string-to-octets xml-data :external-format :utf-8))
		;; if not string, pass directly to rdfxml
		(xml-to-table-inner xml-data))
) ; }}}


(defun debug-table (table) ; debug rdf table {{{
"Print full contents of rdf hash table TABLE for debugging"
	(format t "DEBUG TABLE:~%")
	
	;; loop through items in table
	(maphash #'(lambda (k v)
		(format t "~%ITEM ~a~%" k)
		k  ; leave dummy alone
		
		;; if value is hash table representing item
		;; (it usually should be)
		(when (hash-table-p v)
			(maphash #'(lambda (s o)
				(format t "~a => ~a~%" s o)
			) v))
	)
	table)
) ; }}}


(defun file-to-typed-item (input) ; {{{
	(let (triple-table)
	
	(setq
		;; make an rdf table
		triple-table (conversion-thing-table:xml-to-table input)
	)
	
	triple-table
)) ; }}}
