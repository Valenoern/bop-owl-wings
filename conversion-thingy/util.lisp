;;; utility functions
(defpackage :conversion-thing-util
	(:use :common-lisp)
	(:export
		#:puthash
		#:string-add-line
))
(in-package :conversion-thing-util)


(defun puthash(key value table) "set a hash element (mimic emacs API)" ; {{{
	(setf (gethash key table) value)
) ; }}}

(defun string-add-line (str line) ; {{{
	(format nil "~a~a~%" str line)
) ; }}}
