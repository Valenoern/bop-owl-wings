;;; dictionary table for abbreviating properties  {{{
(defpackage :conversion-thing-dict
	(:use :common-lisp :cl-rdfxml :puri
		:bop-debug :conversion-thing-util
		)
	(:local-nicknames  (:rdfxml :cl-rdfxml))
		;; actual package names aren't really supposed to be prefixed with cl yet this one is
	(:export
		#:property #:property-prefix #:property-name #:property-datatype #:flatten-property
		; #:put-dict-property
		
		#:table-to-dict
		#:debug-dictionary
		;#:wing-bop-short
))
(in-package :conversion-thing-dict)
;;; }}}


;;; property struct - property , create-property , flatten-property {{{

(defstruct property ; prefix name datatype {{{
	prefix
	name
	datatype
) ; }}}
(defun create-property (prefix property) ; short constructor
	(make-property :prefix prefix :name property)
)
(defun flatten-property (property) ; turn property struct to string {{{
	(unless (null property)
		(format nil "~a:~a" (property-prefix property) (property-name property))
)) ; }}}

;; property dictionary

(defun put-dict-property (long prefix name dict) ; populate dict with uri -> PROPERTY - old {{{
	(puthash long (create-property prefix name) dict)
) ; }}}

(defun put-dict-prop2 (subject dict property) ; populate dict with uri -> PROPERTY {{{
	(let ((prefix (property-prefix property)) (key subject))
	(when (typep key 'puri:uri)
		(setq key (puri:render-uri subject nil))
	)
	(when (typep prefix 'puri:uri)
		(setf (property-prefix property) (puri:render-uri prefix nil))
	)
	(puthash key property dict)
)) ; }}}

(defun wing-bop-short () ; short property converter table - old {{{
"temporary way to shorten properties. may become a 'real owlwing' eventually"
	(let ((dict (make-hash-table :test 'equal)))
	
	(put-dict-property "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" "rdf" "type" dict)
	(put-dict-property "http://distributary.network/owl-wing/2021/bop#created" "b" "created" dict)
	(put-dict-property "http://distributary.network/owl-wing/2021/bop#title" "b" "title" dict)
	(put-dict-property "http://distributary.network/owl-wing/2021/bop#name" "b" "name" dict)
	(put-dict-property "http://distributary.network/owl-wing/2021/bop#username" "b" "username" dict)
	(put-dict-property "http://distributary.network/owl-wing/2021/bop#domain" "b" "domain" dict)
	
	dict
)) ; }}}

;;; }}}

(defun cap-prefix (auto-prefix)
	(let (prefix-uri final)
	(setq prefix-uri (puri:render-uri auto-prefix nil))
	(setq final
		(elt prefix-uri (- (length prefix-uri) 1))
	)
	(unless (char= final #\/)
		(setq prefix-uri (format nil "~a#" prefix-uri))
	)
	prefix-uri
))

(defun put-common-properties (dict) ; {{{
;; probably temporary
	(put-dict-prop2 (puri:uri "http://www.w3.org/1999/02/22-rdf-syntax-ns#") dict
		(make-property :prefix "http://www.w3.org/1999/02/22-rdf-syntax-ns#" :name "rdf"
		:datatype (puri:uri "http://www.w3.org/2002/07/owl#Prefix")
	))
	(put-dict-prop2 (puri:uri "http://www.w3.org/1999/02/22-rdf-syntax-ns#type") dict
		(make-property :prefix "http://www.w3.org/1999/02/22-rdf-syntax-ns" :name "type"))
) ; }}}


(defun table-to-dict (table) ; {{{
	(let ((dict (make-hash-table :test 'equal))
		)
	
	(put-common-properties dict)
	
	;; for each item in table
	(maphash #'(lambda (id props)
		(let (subject rdf-type rdfs-range  subj-prefix  prop-name short-prefix)
		id  ; leave dummy alone
		
		;; if value is hash table representing item (it usually should be)
		(when (hash-table-p props)
			;; get basic properties
			(setq subject (gethash "http://www.w3.org/1999/02/22-rdf-syntax-ns#subject" props))
			(setq rdf-type (gethash "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" props))
			(setq rdfs-range (gethash "http://www.w3.org/2000/01/rdf-schema#range" props))
			
			;; try to extract prefix from subject
			(setq subj-prefix (puri:copy-uri subject :fragment nil))
			(setq short-prefix (gethash (puri:render-uri subj-prefix nil) dict))
			(setq short-prefix (if (null short-prefix)
				nil  (property-name short-prefix)
			))
			
			(cond
				;; if this is 'probably' an element or property,
				;; assume it will be named using the fragment (#) of its IRI
				((or
					(puri:uri= rdf-type (puri:uri "http://www.w3.org/2002/07/owl#Class"))
					(puri:uri= rdf-type (puri:uri "http://www.w3.org/2002/07/owl#DatatypeProperty"))
					)
					(setq prop-name (puri:uri-fragment subject))
					
					;; store short prefix in :prefix and no long prefix
					(put-dict-prop2 subject dict
						(make-property :prefix subj-prefix :name prop-name :datatype rdfs-range))
				)
				
				;; if this is a prefix stored in the ontology
				((puri:uri= rdf-type (puri:uri "http://www.w3.org/2002/07/owl#Prefix"))
					(setq prop-name (gethash "http://www.w3.org/2002/07/owl#name" props))
					
					(when (typep prop-name 'literal)
						(setq prop-name (rdfxml:literal-string prop-name))
					)
					
					;; store short prefix in :name and full prefix in :prefix
					(put-dict-prop2 subj-prefix dict
						(make-property :prefix (cap-prefix subj-prefix) :name prop-name :datatype rdf-type))
				)
				
				;; if this is not clearly a definition or prefix just put it in table
				(t  (put-dict-property subject short-prefix prop-name dict))
			)
	)))
	table)
	
	dict  ; return dictionary
)) ; }}}


(defun debug-dictionary (table) ; debug keys and values of dictionary table {{{
"Print full contents of property abbreviator hash table TABLE for debugging"
	(format t "DEBUG TABLE:~%")
	
	;; loop through items in table
	(maphash #'(lambda (key value)
		;; print item IRI and its lisp data type
		(format t "~%ITEM ~a - TYPE ~a~%" key (type-of key))
		;; print value, e.g. schema property information
		(format t "VALUE ~a~%" value)
	)
	table)
) ; }}}
