(defsystem "conversion-thingy"
	:depends-on (
		"uiop" "puri" "cl-rdfxml" "flexi-streams"
		"bop-debug"
	)
	;; bop-debug is just for testing
	:components (
		(:file "util")
		(:file "dictionary" :depends-on ("util"))
		(:file "table" :depends-on ("util"))
		(:file "turtle" :depends-on ("util"))
))
